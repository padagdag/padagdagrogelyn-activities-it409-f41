<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Song;
use App\Models\Playlist;
use App\Models\Playlist_song;


class SongControllers extends Controller
{
    public function addsong(){
        $song = new Song();
        $song->title = request('title');
        $song->artist = request('artist');
        $song->length = request('length');
        $song->save();       
        $id = $song->id;
        $fileName = 'music-'.$id.'.'.request()->song->getClientOriginalExtension();
        request()->song->move(public_path('songs'), $fileName);
        return $id;
    }
    public function displaysong(){
        $song = Song::all();
        return $song;
    }
    public function deletesong(){
        $id = request('id');
        Song::where('id', request('id'))->delete();
        Playlist_song::where('song_id', $id)->delete();
        $path = public_path('songs/music-' . $id);
        $ext = "";
        if(file_exists($path.".mp3")){
            $ext = ".mp3";
        }
        else if(file_exists($path.".wav")){
            $ext = ".wav";
        }
        unlink($path.$ext);
        return;
    }
    public function addplaylist(){
        $playlist = new Playlist();
        $playlist->name = request('name');
        $playlist->save();
        return $playlist->id;
    }
    public function displayplaylist(){
        $displaylist = Playlist::all();
        return $displaylist;
    }
    public function deleteplaylist(){
        $id = request('id');
        Playlist::where('id', request('id'))->delete();
        Playlist_song::where('playlist_id', $id)->delete();
        return;
    }
     public function editplaylist(){
        $id = request('id');
        $name = request('name');
        Playlist::where('id', $id)->update(['name'=>$name]);
        return;
    }
    public function addSongtoplaylist(){
        $playlist_song = new Playlist_song();
        $playlist_song->song_id = request('song_id');
        $playlist_song->playlist_id = request('playlist_id');
        $playlist_song->save();
        return $playlist_song->id;
    }
    public function displaysongtoplaylist($id){
        $results = DB::select('SELECT songs.id, title, length, artist, playlist_songs.id as p_id FROM songs, playlist_songs WHERE playlist_songs.song_id = songs.id and playlist_songs.playlist_id=?', [$id]);
        return $results;
    }
    public function deletesongfromplaylist(){
       $id = request('id');
        Playlist_song::where('id', $id)->delete();
        return;
    }
    public function displaysongnotinplaylist($id){
        $playlist_song = Playlist_song::where('playlist_id', '=', $id)->pluck('song_id')->all();
        $song = Song::whereNotIn('id', $playlist_song)->get();
        return $song;
    }
}
