const app = Vue.createApp({
    data() {
        return {
            modalclosed: null,
            create: "",
            header: "",
            p_index: null,
            playlists: [],
            availablesongs: [],
            music: [],
            test: '',
            musiclist: [],

            title: "none",
            artist: "none",
            album: "none",
            duration: "none"

        }

    },
    methods: {
        upload() {
            var d = this
            var formData = new FormData();
            formData.append('name', this.create);
            axios.post('http://127.0.0.1:8000/addplaylist',
                formData
            ).then(({ data })=> {
                this.playlists.push({
                    id: data,
                    playlist: d.create
                })
                d.create = ""
            }).catch((err)=> {});
            document.getElementById("create-playlist").click()
        },
        drop(e) {
            e.preventDefault()
            this.musiclist = e.dataTransfer.files
        },
        drag(e) {
            e.preventDefault()
        },
        addmusic() {
            var musicfiles = this.musiclist;
            var jsmediatags = window.jsmediatags;
            var d = this;
            jsmediatags.read(musicfiles[0], {
                onSuccess: function (tag) {
                    var info = tag.tags;
                    console.log(info);
                    if (info.title) {
                        d.title = info.title;


                    } else {
                        d.title = musicfiles[0].name;

                    }
                    if (info.artist) {
                        d.artist = info.artist;

                    }
                    if (info.album) {
                        d.album = info.album;

                    }
                    var src = URL.createObjectURL(musicfiles[0]);
                    var audio = document.querySelector("#audio");
                    audio.src = src;
                    audio.onloadedmetadata = function () {
                        d.uploadmusic(d.title, d.artist, audio.duration, musicfiles[0])
                    };
                },
                onError: function (error) {
                    console.log(error);
                }
            });
        },
        uploadmusic(title, artist, length, file){
            var d = this;
            var formData = new FormData();
            formData.append('title', title);
            formData.append('artist', artist);
            formData.append('length', length);
            formData.append('song', file);
            axios.post('http://127.0.0.1:8000/addsong',
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            ).then(({ data })=> {
                d.music.push({
                    id: data,
                    title: title,
                    artist: artist,
                    album: "not set",
                    duration: d.timeduration(length)
                })
                document.querySelector(".cancel").click()
                d.musiclist = []
            }).catch((err)=> {});
        },
        addsongtoplaylist(i){
            var d = this;
            var song_id = this.availablesongs[i].id
            var playlist_id = this.playlists[this.p_index].id
            var formData = new FormData();
            formData.append('song_id', song_id);
            formData.append('playlist_id', playlist_id);
            
            axios.post("http://127.0.0.1:8000/addsongtoplaylist",
                formData
            ).then(({ data })=> {
                d.music.push({
                    id: d.availablesongs[i].id,
                    title: d.availablesongs[i].title,
                    artist: d.availablesongs[i].artist,
                    album: d.availablesongs[i].album,
                    duration: d.availablesongs[i].duration, 
                    p_id: data
                });
                d.availablesongs.splice(i, 1);
            }).catch((err)=> {});
        },
        displaysong(){
            this.p_index = null
            this.header = "All Songs"
            var d = this
            axios.get('http://127.0.0.1:8000/displaysong').then(({ data })=> {
                d.music = []
                for(i=0; i < data.length; i++){
                    d.music.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: "not set",
                        duration: d.timeduration(data[i].length)
                    })
                }
            }).catch((err)=> {});
        },
        displayplaylist(){
            var d = this
            axios.get('http://127.0.0.1:8000/displayplaylist').then(({ data })=> {
                for(i=0; i < data.length; i++){
                    this.playlists.push({
                        id: data[i].id,
                        playlist: data[i].name,
                    })
                }
            }).catch((err)=> {});
        },
        displayplaylistsong(i){
            var d = this;
            this.p_index = i;
            var id = this.playlists[i].id
            var name = this.playlists[i].playlist
            this.header = name
            this.create = name
            axios.get('http://127.0.0.1:8000/displaysongtoplaylist/'+id).then(({ data })=> {
                d.music = []
                for(i=0; i < data.length; i++){
                    d.music.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: "not set",
                        duration: d.timeduration(data[i].length),
                        p_id: data[i].p_id
                    })
                }
            }).catch((err)=> {});
        },
        displayavailablesongs(){
            var d = this;
            var i = this.p_index;
            var id = this.playlists[i].id
            axios.get('http://127.0.0.1:8000/displaysongnotinplaylist/'+id).then(({ data })=> {
                d.availablesongs = []
                for(i=0; i < data.length; i++){
                    d.availablesongs.push({
                        id: data[i].id,
                        title: data[i].title,
                        artist: data[i].artist,
                        album: "not set",
                        duration: d.timeduration(data[i].length),
                    })
                }
            }).catch((err)=> {});
        },
        editplaylist(){
            var i = this.p_index
            var d = this
            var id = this.playlists[i].id
            var name = this.create
            var formData = new FormData();
            formData.append('id', id);
            formData.append('name', name);
            axios.post('http://127.0.0.1:8000/editplaylist',
                formData
            ).then(({ data })=> {
                d.playlists[i].playlist = d.create
                d.header = d.create
                d.create = ""
            }).catch((err)=> {});
        },
        deletesong(i){
            var d = this
            var endpoint = ""
            var id = ""
            if(this.p_index == null){
                endpoint = "deletesong"
                id = this.music[i].id
            }
            else{
                endpoint = "deletesongfromplaylist"
                id = this.music[i].p_id
            }

            var formData = new FormData();
            formData.append('id', id);
            axios.post("http://127.0.0.1:8000/"+ endpoint,
                formData
            ).then(({ data })=> {
                d.music.splice(i, 1)
            }).catch((err)=> {});
        },
        deleteplaylist(){
            var i = this.p_index
            var d = this
            var formData = new FormData();
            var id = this.playlists[i].id
            formData.append('id', id);
            axios.post('http://127.0.0.1:8000/deleteplaylist',
                formData
            ).then(({ data })=> {
                d.playlists.splice(i,1)
                d.displaysong();
            }).catch((err)=> {});
        },
         timeduration(duration) {
            // Hours, minutes and seconds
            var hrs = ~~(duration / 3600);
            var mins = ~~((duration % 3600) / 60);
            var secs = ~~duration % 60;

            // Output like "1:01" or "4:03:59" or "123:03:59"
            var ret = "";

            if (hrs > 0) {
                ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
            }

            ret += "" + mins + ":" + (secs < 10 ? "0" : "");
            ret += "" + secs;
            return ret;
        }
    },
    beforeMount(){
        this.displaysong()
        this.displayplaylist()
    }
});

app.mount("#mp3")
